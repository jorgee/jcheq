# Autor: Jorge Sivil <jsivil@unpaz.edu.ar>

import os
import datetime


class Logger:

    DISABLED = -1
    WARN = 0
    ERROR = 1
    INFO = 2
    DEBUG = 3
    TRACE = 4

    LEVELS_WORD = [
        b'WARN',
        b'ERROR',
        b'INFO',
        b'DEBUG',
        b'TRACE'
    ]

    def __init__(self, filepath, operation_mode=1, max_file_size=1024, max_log_files=5, loglevel=1):
        self.logfile_path = filepath
        self.logfile_fd = open(filepath, 'ab')
        # Mode:
        #  0: Guardar enseguida. Tener en cuenta que se llama a rotate() siempre al inicio.
        #  1: Guardar en self.data, debe ser manualmente grabado al final del uso con flush().
        self.operation_mode = operation_mode
        # Maxsize:
        #  Cantidad máxima en bytes. Para no cortar los logs, el tamaño puede excederse según el string que se guarda.
        self.max_file_size = max_file_size
        self.max_log_files = max_log_files
        self.data = b''
        self.loglevel = loglevel

    def log(self, msg, msg_loglevel, pre_text=''):
        if msg_loglevel <= self.loglevel:
            time = str(datetime.datetime.now()).encode('utf-8')
            msg = msg if isinstance(msg, bytes) else msg.encode('utf-8')
            if pre_text != '':
                msg = pre_text.encode('utf-8') + time + b' - ' + Logger.LEVELS_WORD[msg_loglevel] + b': ' + msg + b'\n'
            else:
                msg = time + b' - ' + Logger.LEVELS_WORD[msg_loglevel] + b': ' + msg + b'\n'
            if self.operation_mode == 0:
                self.log_rotate()
                self.logfile_fd.write(msg)
            else:
                self.data += msg

    def flush(self):
        try:
            self.log_rotate()
            self.logfile_fd.write(self.data)
        except AttributeError as e:
            pass

    def log_rotate(self):
        file_size = os.fstat(self.logfile_fd.fileno()).st_size
        if file_size >= self.max_file_size * 1024:  # kb a bytes
            self.logfile_fd.close()
            for i in range(self.max_log_files - 1, 0, -1):
                current_name = self.logfile_path + '.' + str(i)
                if os.path.isfile(current_name):
                    os.rename(current_name, self.logfile_path + '.' + str(i + 1))
            os.rename(self.logfile_path, self.logfile_path + '.1')
            self.logfile_fd = open(self.logfile_path, 'ab')


class LoggerFactory:

    loggers = {}

    def __init__(self):
        pass

    @staticmethod
    def get_logger(filepath, operation_mode=1, max_file_size=1024, max_log_files=5, loglevel=1):
        if filepath not in LoggerFactory.loggers:
            logger = Logger(filepath, operation_mode, max_file_size, max_log_files, loglevel)
            LoggerFactory.loggers[filepath] = logger
        return LoggerFactory.loggers[filepath]
