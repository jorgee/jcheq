# Autor: Jorge Sivil <jsivil@unpaz.edu.ar>
import os
import unicodedata
from exceptions import exceptions
from logger import logger


class Formatter:

    @staticmethod
    def format(value, params):
        raise NotImplementedError


# Source: Internet
#  Adaptado a Py3, modificado + bugfixes por Jorge Sivil.
#  TODO: Rehacer más prolijo
class ImporteAString(Formatter):
    unidades = [
        'uno',
        'dos',
        'tres',
        'cuatro',
        'cinco',
        'seis',
        'siete',
        'ocho',
        'nueve',
        'diez',
        'once',
        'doce',
        'trece',
        'catorce',
        'quince',
        'dieciséis',
        'diecisiete',
        'dieciocho',
        'diecinueve',
        'veinte',
    ]
    decenas = [
        'veinti',
        'treinta',
        'cuarenta',
        'cincuenta',
        'sesenta',
        'setenta',
        'ochenta',
        'noventa',
        'cien'
    ]
    centenas = [
        'ciento',
        'doscientos',
        'trescientos',
        'cuatrocientos',
        'quinientos',
        'seiscientos',
        'setecientos',
        'ochocientos',
        'novecientos',
    ]

    def __init__(self):
        pass

    @staticmethod
    def format(value, params):
        Helper.log_formatter_info('ImporteAString', value, params)

        importe_en_letras = ImporteAString.__numero_a_string(value)

        if 'transformar_mayus' in params and params['transformar_mayus']:
            importe_en_letras = importe_en_letras.upper()

        if 'utilizar_tildes' in params and not params['utilizar_tildes']:
            importe_en_letras = Helper.strip_accents(importe_en_letras)

        Helper.log_formatter_usage('Valor formateado: ' + importe_en_letras, logger.Logger.DEBUG)
        return StringAMultilinea.format(importe_en_letras, params)

    @staticmethod
    def __numero_a_string(value):
        converted = ''

        if type(value) != 'str':
            number = str(value)
        else:
            number = value

        number_str = number

        try:
            number_int, number_dec = number_str.split('.')
        except ValueError:
            number_int = number_str
            number_dec = ''

        number_str = number_int.zfill(9)
        millones = number_str[:3]
        miles = number_str[3:6]
        cientos = number_str[6:]

        if number_int == '0':
            converted += 'cero '

        if millones == '001':
            converted += 'un millón '
        elif int(millones) > 0:
            converted += '{0} millones '.format(ImporteAString.__convert_number(millones))

        if miles == '001':
            converted += 'un mil '
        elif int(miles) > 0:
            converted += '{0} mil '.format(ImporteAString.__convert_number(miles, True))

        if cientos == '001':
            converted += 'uno '
        elif int(cientos) > 0:
            converted += '{0} '.format(ImporteAString.__convert_number(cientos))

        if number_dec == '':
            number_dec = '00'
        if len(number_dec) < 2:
            number_dec += '0'

        converted += 'con ' + number_dec + '/100'

        return converted

    @staticmethod
    def __convert_number(n, miles=False):
        output = ''

        if n == '100':
            return 'cien'
        elif n[0] != '0':
            output = ImporteAString.centenas[int(n[0]) - 1]
            if n[1] != '0' or n[2] != '0':
                output += ' '

        k = int(n[1:])
        if 0 < k <= 20:
            unidad = ImporteAString.unidades[k - 1]
            if miles and unidad == 'uno':
                unidad = 'un'
            output += unidad
        elif 20 < k < 30:  # Veintena
            unidad = ImporteAString.unidades[int(n[2]) - 1]
            if n[0] == 0 and unidad == 'uno':
                unidad = 'un'
            if miles and unidad == 'uno':
                unidad = 'ún'
            if unidad == 'dos':
                unidad = 'dós'
            if unidad == 'tres':
                unidad = 'trés'
            if unidad == 'seis':
                unidad = 'séis'
            output += '{0}{1}'.format(ImporteAString.decenas[int(n[1]) - 2], unidad)
        elif k > 30 and n[2] != '0':
            unidad = ImporteAString.unidades[int(n[2]) - 1]
            if miles and unidad == 'uno':
                unidad = 'un'
            output += '{0} y {1}'.format(ImporteAString.decenas[int(n[1]) - 2], unidad)
        elif k > 0:
            output += ImporteAString.decenas[int(n[1]) - 2]

        return output


class FechaIso8601AString(Formatter):
    def __init__(self):
        pass

    @staticmethod
    def format(value, params):
        Helper.log_formatter_info('FechaIso8601AString', value, params)

        if value == 0:
            return False

        value = str(value)

        año = int(value[0:4])
        mes = int(value[4:6])
        dia = int(value[6:8])

        meses = {
            1: 'enero',
            2: 'febrero',
            3: 'marzo',
            4: 'abril',
            5: 'mayo',
            6: 'junio',
            7: 'julio',
            8: 'agosto',
            9: 'septiembre',
            10: 'octubre',
            11: 'noviembre',
            12: 'diciembre'
        }

        if 'anio_corto' in params and params['anio_corto']:
            año = int(str(año)[2:4])

        if 'mes_prefijo_de' in params and params['mes_prefijo_de']:
            mes = 'de ' + str(meses[mes])

        if 'anio_prefijo_de' in params and params['anio_prefijo_de']:
            año = 'de ' + str(año)

        data = [{'val': dia}, {'val': mes}, {'val': año}]

        Helper.log_formatter_usage('Valor formateado: ' + str(data), logger.Logger.DEBUG)
        return data


class StringAMultilinea(Formatter):
    def __init__(self):
        pass

    @staticmethod
    def format(value, params):
        Helper.log_formatter_info('StringAMultilinea', value, params)

        if 'cant_lineas' not in params:
            raise exceptions.MissingRequiredParam('Cantidad de líneas no especificada.')

        if 'utilizar_tildes' in params and not params['utilizar_tildes']:
            value = Helper.strip_accents(value)

        data = []
        chars_counter = 0
        for i in range(1, int(params['cant_lineas']) + 1):

            id_str_linea_actual = 'chars_linea' + str(i)
            if id_str_linea_actual not in params:
                raise exceptions.MissingRequiredParam(
                    'Faltan parámetros de cant. de caracteres/linea. Se requiere: \'' + id_str_linea_actual + '\'')

            maxchars_linea_actual = int(params[id_str_linea_actual])
            string_linea = value[chars_counter:chars_counter + maxchars_linea_actual]  # Substring hasta el maxchar indicado

            if len(value[:chars_counter + maxchars_linea_actual]) < len(value):  # Aún restan datos para otras líneas
                pos_espacio = string_linea.rfind(' ')
                if pos_espacio < 0:  # No se encontró un espacio
                    pos_espacio = len(string_linea)
                string_linea = string_linea[:pos_espacio]  # Redondear al último espacio para no cortar palabras a la mitad

            if i == int(params['cant_lineas']):  # Es la última línea
                if 'truncar' in params and not params['truncar']:
                    if len(value[chars_counter:]) > maxchars_linea_actual:
                        raise exceptions.LastLineTooLarge('Texto demasiado largo en última línea. Truncar: false, Maxchars: ' +
                                                          str(maxchars_linea_actual) + ', valor: ' + value[chars_counter:])

            chars_counter = chars_counter + len(string_linea) + 1  # Empezar desde espacio + 1

            if 'completar_guiones' in params and params['completar_guiones']:
                if len(value[:chars_counter]) == len(value):
                    string_linea = string_linea.ljust(maxchars_linea_actual, '-')

            data.append({'val': string_linea})

        Helper.log_formatter_usage('Valor formateado: ' + str(data), logger.Logger.DEBUG)
        return data


class Helper:
    def __init__(self):
        pass

    @staticmethod
    def strip_accents(s):
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

    @staticmethod
    def log_formatter_usage(msg, loglevel):
        parent_dir = os.path.dirname(os.path.realpath(__file__ + '/..'))
        logger_i = logger.LoggerFactory.get_logger(parent_dir + '/logs/jcheq.log')
        logger_i.log(msg, loglevel)

    @staticmethod
    def log_formatter_info(formatter_name, value, params):
        Helper.log_formatter_usage('Formatter: ' + formatter_name, logger.Logger.DEBUG)
        Helper.log_formatter_usage('Valor: ' + str(value), logger.Logger.DEBUG)
        Helper.log_formatter_usage('Parámetros: ' + str(params), logger.Logger.DEBUG)
