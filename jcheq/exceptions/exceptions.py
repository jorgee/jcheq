# Autor: Jorge Sivil <jsivil@unpaz.edu.ar>


class ChequeraNotFound(Exception):
    pass


class CoordsDataQtyMismatch(Exception):
    pass


class FormatterException(Exception):
    pass


class LastLineTooLarge(FormatterException):
    pass


class MissingRequiredParam(FormatterException):
    pass

