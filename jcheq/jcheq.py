#!/usr/bin/env python3
# coding=utf-8
# Autor: Jorge Sivil <jsivil@unpaz.edu.ar>


import argparse
import json
import os
import traceback
import smtplib
import email.mime.multipart
import email.mime.text
import email.mime.base
import subprocess
import time

from formatters import formatters
from print_drivers import print_drivers
from print_systems import print_systems
from exceptions import exceptions
from logger.logger import LoggerFactory, Logger

logger = None
emailer = None
dir_jcheq = os.path.dirname(os.path.realpath(__file__))


class Config:
    def __init__(self):
        pass


class ArgparseCustomHelpFormatter(argparse.HelpFormatter):
    def __init__(self, *args, **kwargs):
        super(ArgparseCustomHelpFormatter, self).__init__(*args, **kwargs)

    def _format_usage(self, usage, actions, groups, prefix):
        return super(ArgparseCustomHelpFormatter, self)._format_usage(
            usage, actions, groups, prefix if prefix else 'Uso: ')


class JCheq:

    def __init__(self, config_class):
        self.config = config_class()
        self.config_dir = dir_jcheq + '/' + 'config'
        self.formatters = {
            'Importe_a_str': formatters.ImporteAString,
            'FechaISO8601_a_str': formatters.FechaIso8601AString,
            'String_a_multilinea': formatters.StringAMultilinea
        }
        self.print_drivers = {
            'EscP': print_drivers.EscP,
        }
        self.print_systems = {
            'WinLPR': print_systems.WinLPR,
            'DebianLPR': print_systems.LPR,
            'RLPR': print_systems.RLPR,
            'BSDNetCat': print_systems.NetCat
        }

    @staticmethod
    def add_cmdline_args(parser_instance):
        if not isinstance(parser_instance, argparse.ArgumentParser):
            raise Exception('El parser debe ser del tipo argparse.ArgumentParser')

        group = parser.add_argument_group('Opciones')
        group.add_argument('-h', '--help', action='help', help='Mostrar este mensaje de ayuda y salir.')
        group.add_argument('-p', '--path-ch', help='Ruta al directorio que contiene los cheques sin procesar.')
        group.add_argument('-n', '--notificar-errores', action='store_true', default=True,
                           help='Notificar errores por correo.')
        group.add_argument('-s', '--smtp-host', help='Dirección del servidor SMTP.')
        group.add_argument('-S', '--smtp-ssl', action='store_true', help='Utilizar SSL en correo saliente.')
        group.add_argument('-P', '--smtp-port', help='Puerto de SMTP.')
        group.add_argument('-U', '--smtp-user', help='Nombre de usuario del servidor SMTP.')
        group.add_argument('-W', '--smtp-password', help='Contraseña del usuario del servidor SMTP.')
        group.add_argument('-f', '--mail-from', help='Remitente del correo.')
        group.add_argument('-t', '--mail-to', action='append', help='Destinatario del correo.')

    def load_config(self):

        config_file_path = self.config_dir + '/config.json'

        logger.log('Cargando archivo de configuración: ' + config_file_path, Logger.DEBUG)

        with open(config_file_path) as config_file:
            data = json.load(config_file)['config']
            for i in data:
                setattr(self.config, i, data[i])

    def get_print_config(self, cheque_inst):

        cfg_chequera = None
        for chequera in self.config.chequeras:
            if chequera['cod_banco'] == cheque_inst.banco_id \
                    and chequera['cod_sucursal'] == cheque_inst.sucursal_id \
                    and chequera['nro_cuenta'] == cheque_inst.cuenta \
                    and chequera['formato'] == cheque_inst.formato:
                cfg_chequera = chequera  # Aliasing
                break

        if cfg_chequera is None:
            excp = exceptions.ChequeraNotFound(
                'No se encontró una chequera para los datos de banco ({0}), sucursal ({1}), '
                'cuenta ({2}) y formato ({3}) dados.'.format(cheque_inst.banco_id, cheque_inst.sucursal_id,
                                                                         cheque_inst.cuenta, cheque_inst.formato))
            raise excp

        if type(cfg_chequera['cfg_datos_imprimir']) is str:  # Todavía no fue cargada con anterioridad

            coord_impresion_fpath = self.config_dir + '/' + cfg_chequera['cfg_datos_imprimir']
            logger.log('Cargando archivo de configuración de coordenadas: ' + coord_impresion_fpath,
                             Logger.DEBUG)

            with open(coord_impresion_fpath) as config_datos_imprimir:
                cfg_chequera['cfg_datos_imprimir'] = json.load(config_datos_imprimir)['cfg_datos_imprimir']

        return cfg_chequera

    def get_print_data(self, print_data_config, cheque_inst):
        data = []
        for config in print_data_config:
            value = getattr(cheque_inst, config['id_dato'])

            if 'formatter' in config:
                formatter_name = config['formatter']
                formatter_instance = self.formatters[formatter_name]()

                params = config['formatter_params'] if 'formatter_params' in config else {}
                value = formatter_instance.format(value, params)
                if value is False:
                    logger.log('El formatter decidió no procesar este valor. Continuando...', Logger.DEBUG)
                    continue
            else:
                value = [{'val': value}]

            coords = config['coordenadas']
            data_qty = len(value)
            coords_qty = len(coords)

            if data_qty > coords_qty:
                raise exceptions.CoordsDataQtyMismatch('La cantidad de datos es mayor a la cantidad de coordenadas.')

            for i in range(0, data_qty):
                data_part = {
                    'x': coords[i]['x'],
                    'y': coords[i]['y'],
                    'val': value[i]['val']
                }
                data.append(data_part)

        return data

    def run(self):

        logger.log('Buscando archivos con extension: \'' + self.config.ch_extension + '\' en ' + self.config.path_cheques, Logger.DEBUG)
        cheques_disponibles = Helper.find_files(self.config.path_cheques, self.config.ch_extension)
        logger.log('Archivos encontrados: ' + str(cheques_disponibles), Logger.DEBUG)

        for cheque_basename in cheques_disponibles:
            try:
                cheque_path = self.config.path_cheques + cheque_basename
                logger.log('Procesando archivo: ' + cheque_path, Logger.INFO, pre_text='\n')
                with open(cheque_path) as cheque_fp:

                    datos_cheque = [campo.strip() for campo in cheque_fp.read().split('\t')]

                    cheque = Cheque()
                    cheque.set_data(datos_cheque)

                    config_chequera = self.get_print_config(cheque)

                    spool = Spool()
                    spool.title = 'Cheque ' + str(cheque.banco_id) + '/' + str(
                        cheque.cuenta) + ': ' + cheque.beneficiario

                    spool.data_raw = self.get_print_data(config_chequera['cfg_datos_imprimir'], cheque)

                    counter_file_path = dir_jcheq + '/logs/{0}_{1}_{2}_{3}.cont'.format(cheque.banco_id, cheque.sucursal_id, cheque.cuenta, cheque.formato)
                    try:
                        counter_fd = open(counter_file_path, 'r')
                        counter = int(counter_fd.read())
                    except FileNotFoundError:
                        counter_fd = open(counter_file_path, 'w')
                        counter_fd.write(str(1))
                        counter = 1
                    counter_fd.close()

                    eject = False
                    linefeed_qty = 0

                    if 'eject' in config_chequera and config_chequera['eject'] != 0:
                        eject = True if int(config_chequera['eject']) % counter == 0 else eject

                    if 'filas_espacio_4to_cheque' in config_chequera:
                        linefeed_qty = int(config_chequera['filas_espacio_4to_cheque']) if counter % 4 == 0 else 0

                    logger.log('Realizar eject: ' + str(eject), logger.DEBUG)
                    logger.log('Filas adicionales (LF): ' + str(linefeed_qty), logger.DEBUG)

                    driver = config_chequera['driver_impresion']
                    logger.log('Driver de impresión: ' + driver, Logger.DEBUG)
                    driver = self.print_drivers[driver]()
                    driver_proc_params = {'eject': eject, 'linefeed_qty': linefeed_qty}

                    if 'driver_impresion_params' in config_chequera:
                        driver_proc_params.update(config_chequera['driver_impresion_params'])

                    logger.log('Datos sin procesar: ' + str(spool.data_raw), Logger.TRACE)
                    logger.log('Parámetros: ' + str(driver_proc_params), Logger.TRACE)
                    spool.data = driver.process(spool.data_raw, driver_proc_params)
                    logger.log('Spool generado sin errores.', Logger.INFO)
                    logger.log('Dump Spool:\n' + str(spool.data), Logger.TRACE)

                    # Descomentar para guardar el spool generado en algún lado.
                    # with open('/run/shm/spool.spl', 'w+b') as f:
                    #     f.write(spool.data)

                    # Permitir 'dry-run': Hacer el procesamiento de datos pero no enviarlos, para debugging.
                    if config_chequera['sistema_impresion'] != '':

                        printing_system = config_chequera['sistema_impresion']

                        printing_system = self.print_systems[printing_system]()
                        printing_params = config_chequera['sistema_impresion_params']

                        try:
                            printing_system.send(spool, printing_params)
                            dest = self.config.path_cheques_renombrados + os.path.splitext(cheque_basename)[0] + \
                                   self.config.ch_renombrados_extension

                            logger.log('Envio de spool correcto. Renombrando ' + cheque_path + ' a ' + dest,
                                             Logger.INFO)
                            os.rename(cheque_path, dest)
                            with open(counter_file_path, 'w') as counter_fd:
                                counter = str(counter + 1)
                                logger.log('Actualizando contador con valor ' + counter, logger.DEBUG)
                                counter_fd.write(counter)
                        except subprocess.CalledProcessError as cpe:
                            logger.log('El sistema de impresion reporto un error. Spool no enviado.', Logger.ERROR)
                            raise cpe
                    else:
                        logger.log('No hay sistema de impresión configurado (dry-run)', Logger.WARN)

            except Exception as e:
                logger.log(str(e) + '\n\n' + traceback.format_exc() + '\n', Logger.ERROR)
                logger.log('Procesamiento del cheque abortado debido a un error.', Logger.INFO)
                Helper.send_error_email('Hubo un error procesando un cheque. Se continúa con el próximo.' +
                                        '\n\nVerifique el log de la aplicación.')


class Helper:

    send_error_email_delay = 60
    email_notifications_active = False

    @staticmethod
    def find_files(path, extension):
        return [f for f in os.listdir(path) if f.endswith(extension)]

    @staticmethod
    def send_error_email(body):

        if not Helper.email_notifications_active:
            logger.log('Notificaciones de error por email desactivadas.', Logger.INFO)
            return

        if emailer is not None:
            timestamp_file_path = dir_jcheq + '/logs/timestamp.epoch'
            timestamp_actual = int(time.time())

            try:
                timestamp_fd = open(timestamp_file_path, 'r')
            except FileNotFoundError:
                timestamp_fd = open(timestamp_file_path, 'w')

            try:
                timestamp_guardado = int(timestamp_fd.read())
            except ValueError:
                timestamp_guardado = 0

            timestamp_fd.close()

            if timestamp_guardado + Helper.send_error_email_delay <= timestamp_actual:
                try:
                    logger.log('Enviando notificación de error por correo.', Logger.INFO)
                    emailer.send_email('Error en JCheq', body)
                    with open(timestamp_file_path, 'w') as timestamp_fd:
                        timestamp_fd.write(str(timestamp_actual))
                except Exception as e:
                    logger.log('Se produjo un error enviando un correo: ' + str(e), Logger.ERROR)
            else:
                logger.log('Notificación de error por email no enviada. Tiempo mínimo entre alertas: ' +
                           str(Helper.send_error_email_delay) + ' segundos.', Logger.INFO)


class Emailer:

    def __init__(self, config):
        self.config = config

    def send_email(self, subject, message):

        msg = email.mime.multipart.MIMEMultipart()

        msg['From'] = 'Sistema JCheq <' + self.config['mail_from'] + '>'
        msg['To'] = ', '.join(self.config['mail_to'])
        msg['Subject'] = subject

        body = '*** Mensaje automático ***\n\n' + message

        msg.attach(email.mime.text.MIMEText(body, 'plain'))

        # filename = ""
        # attachment = open(filename, "rb")
        # part = email.mime.base.MIMEBase('application', 'octet-stream')
        # part.set_payload((attachment).read())
        # encoders.encode_base64(part)
        # part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

        # msg.attach(part)

        server = smtplib.SMTP(self.config['smtp_host'], self.config['smtp_port'])

        if self.config['smtp_ssl']:
            server.starttls()

        server.login(self.config['smtp_user'], self.config['smtp_password'])
        text = msg.as_string()
        server.sendmail(self.config['mail_from'], self.config['mail_to'], text)
        server.quit()


class Cheque(object):
    def __init__(self):
        self.__banco_id = None
        self.__sucursal_id = None
        self.__cuenta = None
        self.__formato = None
        self.__fecha = None
        self.__fecha_pd = None
        self.__beneficiario = None
        self.__importe = None
        self.mapa = {0: 'banco_id',
                     1: 'sucursal_id',
                     2: 'cuenta',
                     3: 'formato',
                     4: 'numero_cheque',
                     5: 'fecha',
                     6: 'fecha_pd',
                     7: 'beneficiario',
                     8: 'importe'}

    @property
    def banco_id(self):
        return self.__banco_id

    @banco_id.setter
    def banco_id(self, valor):
        self.__banco_id = int(valor)

    @property
    def sucursal_id(self):
        return self.__sucursal_id

    @sucursal_id.setter
    def sucursal_id(self, valor):
        self.__sucursal_id = int(valor)

    @property
    def cuenta(self):
        return self.__cuenta

    @cuenta.setter
    def cuenta(self, valor):
        self.__cuenta = valor

    @property
    def formato(self):
        return self.__formato

    @formato.setter
    def formato(self, valor):
        self.__formato = int(valor)

    @property
    def fecha(self):
        return self.__fecha

    @fecha.setter
    def fecha(self, valor):
        fecha_iso8601 = ''.join(valor.split('-'))
        self.__fecha = int(fecha_iso8601)

    @property
    def fecha_pd(self):
        return self.__fecha_pd

    @fecha_pd.setter
    def fecha_pd(self, valor):
        fecha_iso8601 = ''.join(valor.split('-'))
        self.__fecha_pd = int(fecha_iso8601)

    @property
    def beneficiario(self):
        return self.__beneficiario

    @beneficiario.setter
    def beneficiario(self, valor):
        self.__beneficiario = valor

    @property
    def importe(self):
        return self.__importe

    @importe.setter
    def importe(self, valor):
        self.__importe = float(valor)

    # Esto está atado al orden en que vienen los datos en el cheque. Si ese orden cambia, deben revisarse esta manera de asignar datos.
    def set_data(self, data):
        for i in range(0, len(data)):
            setattr(self, self.mapa[i], data[i])


class Spool:
    def __init__(self):

        self.data = b''
        self.data_raw = []
        self.title = ''
        pass


if __name__ == '__main__':
    try:
        logger = LoggerFactory.get_logger(dir_jcheq + '/logs/jcheq.log', loglevel=Logger.ERROR)  # Desactivar con loglevel Logger.DISABLED
        logger.log('*** Comenzando proceso ***', Logger.INFO, pre_text='\n\n')
        parser = argparse.ArgumentParser(prog='jcheq.py',
                                         usage='%(prog)s [options]\n',
                                         add_help=False,
                                         formatter_class=ArgparseCustomHelpFormatter)
        JCheq.add_cmdline_args(parser)

        logger.log('Cargando config en archivo', Logger.INFO)

        jcheq = JCheq(Config)
        jcheq.load_config()

        logger.log('Cargando config en parámetros', Logger.INFO)
        parser.parse_args(namespace=jcheq.config)

        Helper.send_error_email_delay = int(jcheq.config.tiempo_entre_notificaciones)
        Helper.email_notifications_active = jcheq.config.notificar_errores
        emailer = Emailer({
            'smtp_host': jcheq.config.smtp_host,
            "notificar_errores": jcheq.config.notificar_errores,
            "mail_from": jcheq.config.mail_from,
            "mail_to": jcheq.config.mail_to,
            "smtp_host": jcheq.config.smtp_host,
            "smtp_ssl": jcheq.config.smtp_ssl,
            "smtp_port": jcheq.config.smtp_port,
            "smtp_user": jcheq.config.smtp_user,
            "smtp_password": jcheq.config.smtp_password,
        })

        jcheq.run()

    except Exception as e:
        logger.log(str(e) + '\n\n' + traceback.format_exc() + '\n', Logger.ERROR)
        Helper.send_error_email('Hubo un error irrecuperable.\n\nVerifique el log de la aplicación.')
    finally:
        logger.flush()
